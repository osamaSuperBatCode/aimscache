<?php


/**
*  █████╗ ██╗███╗   ███╗███████╗ ██████╗ █████╗  ██████╗██╗  ██╗███████╗
* ██╔══██╗██║████╗ ████║██╔════╝██╔════╝██╔══██╗██╔════╝██║  ██║██╔════╝
* ███████║██║██╔████╔██║███████╗██║     ███████║██║     ███████║█████╗  
* ██╔══██║██║██║╚██╔╝██║╚════██║██║     ██╔══██║██║     ██╔══██║██╔══╝  
* ██║  ██║██║██║ ╚═╝ ██║███████║╚██████╗██║  ██║╚██████╗██║  ██║███████╗
* ╚═╝  ╚═╝╚═╝╚═╝     ╚═╝╚══════╝ ╚═════╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝
*/

/**
 * AimsCache is used for automatic cache. This is a small guide
 * (PLEASE DO READ IT SANKET)
 *
 * AimsCache should be able to speed the application up. It will 
 * sit between PHP and the PHP web application and watch 
 * what data passes by, so that if there is a duplicate request 
 * in the future (within a set timeframe), then instead of letting
 * the app run its course again, we'll serve a saved/cached copy.
 * This means less database queries and less processing per request (as
 * pulling an object from cache usually takes < 1ms).
 *
 * To implement AimsCache, all you need to do is add a few
 * lines of code to the top (or near the top) of your script.
 * The positioning is irrelevant as long as nothing is shown
 * to the client and no HTTP headers are sent before you call
 * AimsCache::Push() or AimsCache::PullOrPush().
 *
 *    <?php
 *    require 'aims-cache.php';
 *    AimsCache::aimsHash($_SERVER['REQUEST_URI']);
 *    AimsCache::aimsPullOrPush(5);
 *
 * The above script will make any requests with the same URI
 * to be considered the same object. The object will stay
 * cached for a maximum of 5 seconds (that's the hitpoints -- time
 * to live). Any other requests within 5 seconds of the
 * another request of the same URI will be served from cache.
 *
 * If content differs per IP address or cookie, simply
 * use something like this:
 *
 *    <?php
 *    require 'aims-cache.php';
 *    AimsCache::aimsHash($_SERVER['REQUEST_URI']);
 *    // Don't serve the same content for different IP addresses.
 *    AimsCache::aimsHash($_SERVER['REMOTE_ADDR']);
 *    AimsCache::aimsPullOrPush(5);
 * OR
 *    <?php
 *    require 'aims-cache.php';
 *    AimsCache::aimsHash($_SERVER['REQUEST_URI']);
 *    // Different "username" cookie, different object.
 *    AimsCache::aimsHash($_COOKIE['username']);
 *    AimsCache::aimsPullOrPush(5);
 *
 * To remove an object from cache, use ``AimsCache::aimsKill``.
 * AimsCache::aimsKill takes one argument: the hash in array
 * form:
 *
 *    <?php
 *    require 'aims-cache.php';
 *    AimsCache::aimsKill(array($_SERVER['REQUEST_URI']));
 *
 * SECURITY NOTE:
 *    Please ensure that AimsCache's directory (AIMSCACHE_DIR)
 *    is not accessible by the general public. You may
 *    restrict it using .htaccess or use some other
 *    means. If users can execute PHP files in that
 *    directory, they can see objects that may not have been
 *    meant for their eyes.
 *
 */

define('AIMSCACHE_DEFAULT_HITPOINTS', 5);
define('AIMSCACHE_HASH', 'md5');
define('AIMSCACHE_DIR', getcwd().'/aimscache/');

/*Begin Main Magic here*/

class AimsCache {

	public static $key = '';
	public static $hitpoints = AIMSCACHE_DEFAULT_HITPOINTS;
	public static $wrote_header = false;


	/* $data is the string to be added to our hash.*/
	public static function aimsHash($data) {
		self::$key .= $data . '-';
	} 

	/* Create Hash Here*/

	public static function aimsKey($key=false)
	{
		return hash(AIMSCACHE_HASH, $key ? $key : self::$key);
	}

	/**
	* Return the filename of this object. The filename is composed of the
	* hash, followed by ".php".
	*
	* If $timestamp is true, the filename of the accompanying "timestamp"
	* file is returned, which is helpful to AimsCache::AutoKill().
	*/

	public static function aimsFilename($timestamp = false, $key = false)
	{
		return AIMSCACHE_DIR . ($timestamp? time() + self::$hitpoints . '_' :'') . ($key ? $key : self::aimsKey()) . ($timestamp? '' : '.php');
	}


	private static function aimsError($message) {
		return 'Error: ' . $message;
	}

	/**
	 * Begin or continue buffering output for the soon-to-be-cached object.
	 *
	 * If AimsCache::aimsFilename() doesn't exist, AimsCache::aimsWrite() will create
	 * it and begin writing header information.
	 *
	 * The resulting file is simply a .php script which, when ran, will
	 *   a) check whether or not the object is stale (if so, it will remove
	 *      itself);
	 *   b) send the HTTP headers associated with the object; and
	 *   c) send the cached data to the client.
	 *
	 * http://php.net/manual/en/function.ob-start.php
	 *
	 */

	public static function aimsWrite($data)
	{
		$file = self::aimsFilename();

		if(!file_exists(AIMSCACHE_DIR) && !@mkdir(AIMSCACHE_DIR))
			return self::aimsError('Could not create directory: ' . AIMSCACHE_DIR);

		if(!$fp = fopen($file, 'w'))
			return self::aimsError('Unable to open file for writing: ' . $file);

		if(!self::$wrote_header) {
			fwrite($fp, "<?php\n\n");

			// If the object is stale, return instead.
			fwrite($fp, "if(time() >= \$_" . self::aimsKey() . "_time = " . (time() + self::$hitpoints) . ") return;\n");

			// Tell AimsCache that the object was successfully retrieved from cache.
			// If the variable "$_{$key}" is not set after loading the PHP script,
			// AimsCache will assume it quit prematurely because it expired. (see above)
			fwrite($fp, "\$_" . self::aimsKey() . " = true;\n\n");

			// List HTTP headers sent with header() and add them to our object.
			foreach($headers = headers_list() as $header) {
				fwrite($fp, "header(" . var_export($header, true) . ");\n");
			}

			fwrite($fp, "\n?>");

			touch(self::aimsFilename(true));
		}

		fwrite($fp, str_replace('<?', '<?php echo \'<?\'; ?>', $data));

		fclose($fp);

		return $data;
	}

	/**
	 * Callback for register_shutdown_function(). It ends output
	 * buffering and passes the (remaining) data to AimsCache::aimsWrite().
	 *
	 * http://php.net/manual/en/function.register-shutdown-function.php
	 *
	 */
	public static function aimsShutdown() {
		// Give AimsCache (the rest of) the output.
		ob_end_flush();
	}


	/**
	 * Attempt to pull this object from cache. If it doesn't exist or has
	 * since expired, return false.
	 *
	 * If successful, PHP will exit and obviously is returned.
	 *
	 */
	private static function aimsPull() {
		$key = self::aimsKey();

		// Check if the object is cached.
		if(file_exists($file = self::aimsFilename())) {
			unset(${'_' . $key});
			unset(${'_' . $key . '_time'});

			// Include the cached object.
			require $file;

			// Only if successful will this variable be set.
			if(isset(${'_' . $key}))
				exit;

			// The object was either invalid or stale.
			if(isset(${'_' . $key . '_time'})) {
				// Remove assocaited timestamp file.
				unlink(AIMSCACHE_DIR . ${'_' . $key . '_time'} . '_' . self::aimsKey());
			}

			unlink($file);
		}

		return false;
	}


	/**
	 * Begin to write the cache file, letting the web application run its
	 * task normally while buffering all output to AimsCache::aimsWrite().
	 *
	 * If $hp is not set, AIMSCACHE_DEFAULT_HITPOINTS will be used instead.
	 *
	 */
	public static function aimsPush($hp) {
		if($hp !== false)
			self::$hitpoints = $hp;

		ob_start('AimsCache::aimsWrite');
		register_shutdown_function('AimsCache::aimsShutdown');
	}


	/**
	 * As the name suggests, this function will first attempt to pull the
	 * object from cache. If unsuccessful (or expired), we will start to
	 * write the cache file, letting the web application run its task.
	 *
	 * It is the equivalent of
	 *    AimsCache::Pull();
	 *    AimsCache::Push($hitpoints);
	 *
	 */
	public static function aimsPullOrPush($hp = false) {
		// Try pulling the object from cache.
		self::aimsPull();
		// Begin writing the object to cache.
		self::aimsPush($hp);
	}

	/**
	 * Delete a cached object.
	 *
	 */
	public static function aimsKill($hash) {
		$key = '';
		foreach($hash as $h) {
			$key .= $h . '-';
		}

		$key = self::aimsKey($key);

		// Delete object
		@unlink($filename = self::aimsFilename(false, $key));

		// Delete "timestamp" files
		$times = glob(AIMSCACHE_DIR . '/*_' . $key);
		foreach($times as $file) {
			@unlink($file);
		}
	}

	/**
	 * Remove old, stale objects.
	 *
	 * As this function iterates through (the filenames of) all objects,
	 * stale or not, it may be undersirable to use this on every request.
	 *
	 * You should probably include this in an administrative area so it
	 * won't be ran all the time OR let it be a scheduled task (using
	 * crontab, etc.)
	 *
	 */
	public static function AutoKill() {
		if(!file_exists(AIMSCACHE_DIR) && !@mkdir(AIMSCACHE_DIR))
			return;

		if(!$dir = opendir(AIMSCACHE_DIR))
			die(self::aimsError('Unable to open directory for reading: ' . AIMSCACHE_DIR));

		while(($entry = readdir($dir)) !== false) {
			if(preg_match('/^(\d+)_(.+)$/', $entry, $matches)) {
				if(time() > (int)$matches[1]) {
					unlink(AIMSCACHE_DIR . $entry);
					unlink(AIMSCACHE_DIR . $matches[2] . '.php');
				}
			}
		}

		closedir($dir);
	}





}
